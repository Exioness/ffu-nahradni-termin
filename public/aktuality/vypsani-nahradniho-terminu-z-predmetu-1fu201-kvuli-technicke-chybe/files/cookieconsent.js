"use strict";

(function ($, root) {
  $(function () {

    function updateGoogleConsent(serviceConsentList){
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}

      var consentData = {
        'analytics_storage':(serviceConsentList.ga ? 'granted' : 'denied'),
        'ad_storage':(serviceConsentList.gads ? 'granted' : 'denied')
      };
      $.each(serviceConsentList, function(service, consent){
        consentData['service_'+service+'_storage']=(consent ? 'granted' : 'denied');
      });

      gtag('consent', 'update', consentData);
      consentData.event='custom_consent';
      dataLayer.push(consentData);
    }

    /**
     * @param name : string
     * @param value : string
     * @param expirationInDays : number
     */
    function setCookie(name, value, expirationInDays){
      expirationInDays = typeof expirationInDays !== 'undefined' ? expirationInDays : 0;
      var d = new Date();
      d.setTime(d.getTime() + (expirationInDays*24*60*60*1000));
      document.cookie = name+"="+encodeURIComponent(value)+"; "+(expirationInDays!==0?'expires='+d.toUTCString()+'; ':'')+(location.hostname.substr(-7).toLowerCase()==='.vse.cz'?'Domain=.vse.cz; ':'')+"Path=/;SameSite=None; Secure";
    }

    /**
     * @param name : string
     * @return {string}
     */
    function getCookie(name) {
      var ca = decodeURIComponent(document.cookie).split(';');
      for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name+'=') === 0) {
          return c.substring(name.length+1, c.length);
        }
      }
      return "";
    }

    var cookieconsent = $('#cookieconsent');
    cookieconsent.find('button.cookieconsent-accept-all').click(function(e){
      e.preventDefault();
      //uĹľivatel chce povolit vĹˇe
      setCookie('vse_cookie_set','1',90);
      setCookie('vse_cookie_allow','1',90);
      cookieconsent.addClass('d-none');
      var consentServices = {};
      cookieconsent.find('input.cookieconsent-switch-input').each(function(){
        $(this).prop('checked',true);
        consentServices[$(this).val()]=true;
      });
      updateGoogleConsent(consentServices);
    });
    cookieconsent.find('button.cookieconsent-accept-none').click(function(e){
      e.preventDefault();
      //uĹľivatel chce zakĂˇzat vĹˇe
      setCookie('vse_cookie_set','1',90);
      setCookie('vse_cookie_allow','[]',90);
      cookieconsent.addClass('d-none');
      var consentServices = {};
      cookieconsent.find('input.cookieconsent-switch-input').each(function(){
        $(this).prop('checked',true);
        consentServices[$(this).val()]=false;
      });
      updateGoogleConsent(consentServices);
    });
    cookieconsent.find('button.cookieconsent-save-customized').click(function(e){
      e.preventDefault();
      //uĹľivatel uklĂˇdĂˇ vlastnĂ­ nastavenĂ­
      var result = [];
      var consentServices = {};
      cookieconsent.find('.cookieconsent-switch-input').each(function(){
        var cookieService = $(this).val();
        if ($(this).is(':checked')){
          result.push(cookieService);
          consentServices[cookieService]=true;
        }else{
          consentServices[cookieService]=false;
        }
      });
      setCookie('vse_cookie_set','1',90);
      setCookie('vse_cookie_allow',JSON.stringify(result),90);
      cookieconsent.addClass('d-none');
      //nastavenĂ­ stavu GA
      updateGoogleConsent(consentServices);
    });
    cookieconsent.find('button.cookieconsent-customize').click(function(e){
      e.preventDefault();
      //zobrazenĂ­ podrobnĂ©ho nastavenĂ­
      cookieconsent.find('.close').addClass('d-none');
      cookieconsent.find('.cookieconsent-buttons').addClass('d-none');
      cookieconsent.find('.cookieconsent-customization').removeClass('d-none');
    });

    //kontrola, jestli se mĂˇ zobrazit nastavenĂ­ cookies
    var cookieSet = getCookie('vse_cookie_set');
    if (cookieSet!=='1'){
      cookieconsent.removeClass('d-none');
    }

    $('.cookieconsent-show').click(function(e){
      e.preventDefault();
      cookieconsent.removeClass('d-none');
    });

  });
})(jQuery, this);